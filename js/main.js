$(document).ready(function(){
	
	//set the main application variables
	const peopleListURL = 'https://sys4.open-web.nl/employees.json';
	var data;
	var skillFilter = [];
	var queryFilter = '';
	
	
	var renderPeopleList = function(){
		const skillColors = {
			sales: 		'label-danger',
			marketing: 	'label-warning',
			javascript: 'label-success',
			html: 		'label-success',
			css: 		'label-success'
		}
		var items = [];
		var skillList = [];
		var counter = 0;
		
		
		//start looping trough the employees to create the dom elements
		for(var i = 0; i < data.employees.length; i++){
			
			//store local reference
			var employee = data.employees[i];
			
			
			//start filtering. we dont want to show people that dont comply with the selected filters
			var show = true;
			if(skillFilter.length > 0){
				for(var j = 0; j < skillFilter.length; j++){
					if(employee.skills.indexOf(skillFilter[j]) == -1) show = false;
				}
			}
			if(queryFilter){
				var personSearch = employee.name + employee.bio + employee.role;
				if(personSearch.toLowerCase().indexOf(queryFilter.toLowerCase()) == -1) show = false;
			}
			if(!show) continue;
			
			
			//the filters say Ok�, so we can start creating the dom tree
			var bioSplit = employee.bio.split(' ').slice(0, 15).join(' ');
			var skills = '';
			
			for(var j = 0; j < employee.skills.length; j++){
				var skill = employee.skills[j];
				skills += '<span class="label '+(skillColors[skill] ? skillColors[skill] : 'label-primary')+'">'+employee.skills[j]+'</span> ';
				
				if(skillList.indexOf(skill) == -1) skillList.push(skill);
			}
			
			var item = '<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">'+
							'<div class="panel panel-default">'+
								'<div class="panel-body">'+
									'<div class="container-fluid no-padding">'+
										'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-4 no-padding">'+
											'<img src="'+employee.profileImage+'" alt="'+employee.name+'" class="avatar img-rounded">'+
										'</div>'+
										'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">'+
											'<h5>'+employee.name+'<br /><span class="text-muted">'+employee.role+'</span></h5>'+
											'<p>'+bioSplit+' <a class="bioButton" index="'+i+'">Read more</a></p>'+
											'<p>'+skills+'</p>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
			
			items.push(item);
			
			
			//add clearfixes for different screensizes
			counter ++;
			if((counter / 6) % 1 == 0) items.push('<div class="clearfix visible-lg-block"></div>');
			if((counter / 6) % 1 == 0) items.push('<div class="clearfix visible-md-block"></div>');
			if((counter / 4) % 1 == 0) items.push('<div class="clearfix visible-sm-block"></div>');
			if((counter / 1) % 1 == 0) items.push('<div class="clearfix visible-xs-block"></div>');
		};
		$('#peopleList').html(items.join(''));
		
		
		//enable the bio buttons
		$('#peopleList .bioButton').click(function(){
			
			var index = parseInt($(this).attr('index'));
			var employee = data.employees[index];
			
			$('#bioModal .modal-header').html(employee.name);
			$('#bioModal .modal-body').html(employee.bio);
			$('#bioModal').modal('show');
		})
		
		return skillList;
	}
	
	
	var initFilters = function(skillList){
		console.info('Skill list: '+skillList.join(', '));
		
		
		//create the skills filter
		var skillOptions = '';
		for(var i = 0; i < skillList.length; i++){
			skillOptions += '<option value="'+skillList[i]+'">'+skillList[i]+'</option>';
		}
		$('#skillFilter').html(skillOptions).multiselect({
            buttonContainer: '<div id="multiselect-container"></div>',
            onChange: function(option, checked) {
	            
	            var value = $(option[0]).attr('value');
	            if(checked){
		            if(skillFilter.indexOf(value) == -1) skillFilter.push(value);
	            }else{
		            skillFilter.splice(skillFilter.indexOf(value), 1);
	            }
	            renderPeopleList();
            }
        });
        
        
        //create the query filter
        $('#queryFilter').keyup(function(){
	        queryFilter = $('#queryFilter').val();
	        renderPeopleList();
        })
	}
	
	
	$.getJSON(peopleListURL, null, function(receivedData, textStatus, jqXHR){
		data = receivedData; 	//store the data
		console.info(data);
		
		
		var skillList = renderPeopleList(); 	//render the list
		initFilters(skillList); 				//init the filters
	})
})